package com.dgg;

import java.awt.*;

public class Player {

    private int helth = 100;
    private int radius;
    private int x;
    private int y;
    private int speed = 5;
    private Color color;
    private int attackRange = 100;
    private boolean isAttack = false;
    private long currentAttackTime;
    private long previousAttackTime;
    private long cooldown = 2000;
    private boolean left, right, down, up;

    public Player(int helth, int radius, int x, int y, Color color) {
        this.helth = helth;
        this.radius = radius;
        this.x = x;
        this.y = y;
        this.color = color;
    }

    public int getHelth() {
        return helth;
    }

    public void setHelth(int helth) {
        this.helth = helth;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public long getCurrentAttackTime() {
        return currentAttackTime;
    }

    public void setCurrentAttackTime(long currentAttackTime) {
        this.currentAttackTime = currentAttackTime;
    }

    public long getPreviousAttackTime() {
        return previousAttackTime;
    }

    public void setPreviousAttackTime(long previousAttackTime) {
        this.previousAttackTime = previousAttackTime;
    }

    public long getCooldown() {
        return cooldown;
    }

    public void setCooldown(long cooldown) {
        this.cooldown = cooldown;
    }

    public boolean isAttack() {
        return isAttack;
    }

    public void setAttack(boolean attack) {
        isAttack = attack;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getAttackRange() {
        return attackRange;
    }

    public void setAttackRange(int attackRange) {
        this.attackRange = attackRange;
    }

    public void draw(Graphics2D g2d){
        g2d.setColor(color);
        g2d.fillOval(x - radius, y - radius,
                radius * 2, radius * 2);
        if (checkAttack()){
            drawAttack(g2d);
        }
    }

    private void drawAttack(Graphics2D g2d){
        g2d.setColor(Color.BLACK);
        g2d.fillOval(x - attackRange, y - attackRange, attackRange * 2, attackRange * 2);
        if (System.currentTimeMillis() - currentAttackTime > 100) {
            setPreviousAttackTime(System.currentTimeMillis());
            setAttack(false);
        }
    }

    private boolean checkAttack(){
        return isAttack() && currentAttackTime - previousAttackTime > cooldown;
    }

    public void update(){
        move();
    }

    public void move(){
        if (left) x -= speed;
        if (right) x += speed;
        if (up) y -= speed;
        if (down) y += speed;
    }

    public void setLeft(boolean left) {
        this.left = left;
    }

    public void setRight(boolean right) {
        this.right = right;
    }

    public void setDown(boolean down) {
        this.down = down;
    }

    public void setUp(boolean up) {
        this.up = up;
    }
}
