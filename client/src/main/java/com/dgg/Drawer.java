package com.dgg;

import java.awt.*;
import java.awt.image.BufferStrategy;

public class Drawer extends Canvas {

    private Player player;
    private Player enemy;

    public Drawer(Player player, Player enemy) {
        this.player = player;
        this.enemy = enemy;
    }

    public void render(){
        BufferStrategy bs = getBufferStrategy();
        if (bs == null){
            createBufferStrategy(2);
            bs = getBufferStrategy();
        }
        Graphics2D g2d = (Graphics2D) bs.getDrawGraphics();
        g2d.setColor(Color.white);
        g2d.fillRect(0,0, getWidth(), getHeight());
        player.draw(g2d);
        enemy.draw(g2d);
        g2d.dispose();
        bs.show();
    }

}
