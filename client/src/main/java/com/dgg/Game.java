package com.dgg;

import javax.swing.*;
import java.awt.*;

public class Game {

    public static void main(String[] args) throws InterruptedException {

        Player player = new Player(100, 50, 0, 0, Color.RED);
        Player enemy = new Player(100, 50, 300, 300, Color.MAGENTA);
        Drawer drawer = new Drawer(player, enemy);
        KeyboardControler keyboardControler = new KeyboardControler(player);

        JFrame window = new JFrame("Our Game");
        window.setSize(800, 600);
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        window.setVisible(true);
        drawer.addKeyListener(keyboardControler);
        window.add(drawer);
        window.addKeyListener(keyboardControler);

        window.repaint();

        while (true){
            player.update();
            drawer.render();
            Thread.sleep(1000 / 60);
        }

    }


}
